const store = require('../../store/account-mock.json');

const accountController = {
    getUserAction: (req, res) => {
        res.json(store.account);
    }
};

module.exports = accountController;