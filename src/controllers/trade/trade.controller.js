const store = require('../../store/account-mock.json');
const WAValidator = require('wallet-address-validator');
const CoinKey = require('coinkey')

const getRandomAdrress = () => {
    return CoinKey.createRandom().publicAddress;
}

const transactionOK = (req, res) => {
    
    if (req.body.address && WAValidator.validate(req.body.address)) {
        const total = req.body.amount + req.body.fee;
        store.account = {
            ...store.account,
            wallets: {
                BTC: {
                    balance: store.account.wallets.BTC.balance - total,
                    address: getRandomAdrress()
                }
            }
        }
        res.status(200).json({
            transaction: 'OK',
            reason: 'RANDOM API 200 status',
            account: store.account
        });
    } else {
        res.status(400).json({
            transaction: 'FAIL',
            reason: 'Invalid BTC Address'
        });
    }
};

const transactionTimeout = (req, res) => {
    res.status(504).json({
        transaction: 'FAIL',
        reason: 'RANDOM API 504 error'
    });
}

const transactionTimeoutNoResponse = (req, res) => {
    res.status(504);
}

const availableActions = [transactionOK, transactionTimeout, transactionTimeoutNoResponse];

const tradeController = {
    sendBTCAction: (req, res) => {

        switch(req.headers["x-force-response"]) {
            case 'OK':
                transactionOK(req, res);
            case 'TIMEOUT':
                transactionTimeoutNoResponse(req, res);
                break;
            default: 
                var random = Math.floor(Math.random() * availableActions.length);
                availableActions[random](req, res);
        };

        // 
    }
};

module.exports = tradeController;