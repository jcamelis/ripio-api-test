const express = require('express');
const accountController = require('./src/controllers/account/');
const tradeController = require('./src/controllers/trade/');


const app = express();

app.use(express.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if( req.method === 'OPTIONS') {
        res.status(200).send({});
    } else {
        next();
    }
});

app.get('/user/', accountController.getUserAction);
app.post('/trade/send/BTC/', tradeController.sendBTCAction);
  
app.listen(3001);